

class Customer {
    constructor(email){
        this.email= email,
        this.cart = new Cart,
        this.orders = [];
    }

    checkOut() {
        if(this.cart.contents.length > 0){
            this.orders.push({
                products: this.cart.contents,
                totalAmount: this.cart.totalAmount
            })
        }
        return this;
    }
}

class Cart {
    constructor(){
        this.contents = [];
        this.totalAmount = 0;
    }
   
    addToCart(product, num) {
        this.contents.push({
            product,
            quantity: num
        })
        return this;
    }

    showCartContents() {
        console.log(this.contents)
        return this;
    }

    updateproductQuantity(name, num){
        this.contents.find(content=>content.product.name === name).quantity = num;
        // this.contents.find(name).quantity = num
        return this;
    }
    clearCartContents() {
        this.contents = [];
        return this;
    }

    computeTotal() {
        let total = 0;
        console.log(this.contents)
        this.contents.forEach((content)=>{
            if(content){
                total =  content.product.price * content.quantity;
            }
        })
        this.totalAmount = total;
        return this;
    }

}

class Product {
    constructor(name, price){
        this.name = name;
        this.price = price;
        this.isActive = true;
    }

    archive(){
        return this;
    }
    updatePrice(newPrice){
        this.price = newPrice;
        return this;
    }
}

const john = new Customer('john@mail.com')
console.log(john)

const prodA = new Product("soap", 9.99)

console.log(prodA)

// prodA.updatePrice(12.99)

console.log(prodA)

john.cart.addToCart(prodA, 3)

console.log(john)

john.cart.showCartContents()

// console.log(john.cart.updateproductQuantity('soap', 5))

// console.log(john.cart.clearCartContents())
john.cart.computeTotal()

console.log(john.cart.totalAmount)

john.checkOut()
console.log(john)